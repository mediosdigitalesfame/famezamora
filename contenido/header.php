<header class="clearfix">
	<!-- Static navbar -->
	<div class="navbar navbar-default navbar-fixed-top">
		<div class="top-line">
			<div class="container">
				<p>
					
					<span><i class="fa fa-phone"></i>Agencia: (351) 512 1640</span>
				</p>
				<ul class="social-icons">
					<li><a class="facebook" href="https://www.facebook.com/Chevrolet-FAME-Zamora-2046315865462062/" target="_blank"><i class="fa fa-facebook"></i></a></li>
					<li><a class="twitter" href="https://twitter.com/chevroletfame" target="_blank"><i class="fa fa-twitter"></i></a></li>
					<li><a class="youtube" href="https://www.youtube.com/user/GrupoFameAutos" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
					<li>
						<a class="whatsapp" href="https://api.whatsapp.com/send?phone=524433952597&text=Hola,%20Quiero%20más%20información!" target="_blank">
							<i class="fa fa-whatsapp"></i>
						</a>
					</li>
					<li><a class="instagram" href="https://www.instagram.com/grupofame/" target="_blank"><i class="fa fa-instagram"></i></a></li>
				</ul>
			</div>
		</div>
		
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.php" target="_self"><img alt="Inicio" src="images/logo.png"></a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">

					<li><a class="active" href="index.php">Inicio</a></li>
					<li class="drop"><a href="autos.php">Autos</a>
						<ul class="drop-down">
							<li><a href="autos.php" target="_self">Autos</a></li>
							<li><a href="autos.php" target="_self">SUV</a></li>
							<li><a href="autos.php" target="_self">Pick Up's</a></li>
							<li><a href="autos.php" target="_self">Comerciales</a></li>																																				
						</ul> </li>                             
						
						<li class="drop"><a href="servicio.php">Servicio</a>
							<ul class="drop-down">
								<li><a href="servicio.php">Cita de Servicio</a></li>                     
								<li><a href="refacciones.php">Refacciones</a></li>
								<li><a href="garantia.php">Garantía</a></li>
							</ul> </li>                                                             
							
							<li class="drop"><a href="promociones.php">Promociones</a>
								<ul class="drop-down">
									<li><a href="autofinanciamiento.php">Financiamiento</a></li>
									<li><a href="servicio.php">Servicio</a></li>
								</ul>  </li>      
								<li><a href="http://fameseminuevos.com/" target="_blank">Seminuevos</a></li>                      
								<li><a href="contacto.php">Contacto</a></li>
								<li><a href="ubicacion.php">Ubicación</a></li>
							</ul>
						</div>
					</div>
				</div>

				 
			</header>
