<!-- CONTENEDOR COCHES -->
		<div id="content">
			<!-- Modelos 2016 -->
			<div class="latest-post">
				<div class="title-section">
					<h1>Nuestros <span> Vehículos</span></h1>
					<p>Navega a través de la sección para seleccionar el auto de tu preferencia</p>
				</div>
                
				<div id="owl-demo" class="owl-carousel owl-theme">
         			
	               

 					  <div class="item news-item">
	                   	 <center>
							 <a href="pdfs/Spark2018.pdf" target="_blank"><img alt="Spark 2018" src="images/autos/spark2018.jpg"></a>
							  <h2> <strong> <em> SPARK <sup>&reg;</sup> <font color="#f5bc05">2018</font> </em> </strong></h2> </font>  
							 <a class="read-more" href="pdfs/spark2018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                     </center>
 					 </div>

 					  <div class="item news-item">
	                   	 <center>
							 <a href="pdfs/cavalier2018.pdf" target="_blank"><img alt="Cavalier 2019" src="images/autos/cavalier2019.jpg"></a>
							  <h2> <strong> <em> CAVALIER <sup>&reg;</sup> <font color="#f5bc05">2019</font> </em> </strong></h2> </font>  
							 <a class="read-more" href="pdfs/cavalier2019.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                     </center>
 					 </div>

 					 <div class="item news-item">
	                   	 <center>
							 <a href="pdfs/beathb2018.pdf" target="_blank"><img alt="Beat HB 2018" src="images/autos/beat2018.jpg"></a>
							  <h2> <strong> <em> BEAT HB <sup>&reg;</sup> <font color="#f5bc05">2018</font> </em> </strong></h2> </font>  
							 <a class="read-more" href="pdfs/beathb2018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                     </center>
 					 </div>

 					 <div class="item news-item">
	                   	 <center>
							 <a href="pdfs/aveo2018.pdf" target="_blank"><img alt="Aveo 2018" src="images/autos/aveo2018.jpg"></a>
							  <h2> <strong> <em> AVEO <sup>&reg;</sup> <font color="#f5bc05">2018</font> </em> </strong></h2> </font>  
							 <a class="read-more" href="pdfs/aveo2018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                     </center>
 					 </div>

 					 <div class="item news-item">
	                   	 <center>
							 <a href="pdfs/beatnb2019.pdf" target="_blank"><img alt="Beat NB 2019" src="images/autos/beatnb2019.jpg"></a>
							  <h2> <strong> <em> BEAT NB <sup>&reg;</sup> <font color="#f5bc05">2019</font> </em> </strong></h2> </font>  
							 <a class="read-more" href="pdfs/beatnb2019.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                     </center>
 					 </div>

 					 <div class="item news-item">
	                   	 <center>
							 <a href="pdfs/malibu2018.pdf" target="_blank"><img alt="Malibu 2018" src="images/autos/malibu2018.jpg"></a>
							  <h2> <strong> <em> MALIBU <sup>&reg;</sup> <font color="#f5bc05">2018</font> </em> </strong></h2> </font>  
							 <a class="read-more" href="pdfs/malibu2018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                     </center>
 					 </div>

 					 <div class="item news-item">
	                   	 <center>
							 <a href="pdfs/cheyenne2018.pdf" target="_blank"><img alt="Cheyenne 2018" src="images/autos/cheyenne2018.jpg"></a>
							  <h2> <strong> <em> CHEYENNE <sup>&reg;</sup> <font color="#f5bc05">2018</font> </em> </strong></h2> </font>  
							 <a class="read-more" href="pdfs/cheyenne2018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                     </center>
 					 </div>

 					 <div class="item news-item">
	                   	 <center>
							 <a href="pdfs/suburban2018.pdf" target="_blank"><img alt="Suburban 2018" src="images/autos/suburban2018.jpg"></a>
							  <h2> <strong> <em> SUBURBAN <sup>&reg;</sup> <font color="#f5bc05">2018</font> </em> </strong></h2> </font>  
							 <a class="read-more" href="pdfs/suburban2018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                     </center>
 					 </div>

 					 <div class="item news-item">
	                   	 <center>
							 <a href="pdfs/tahoe2018.pdf" target="_blank"><img alt="Tahoe 2018" src="images/autos/tahoe2018.jpg"></a>
							  <h2> <strong> <em> TAHOE <sup>&reg;</sup> <font color="#f5bc05">2018</font> </em> </strong></h2> </font>  
							 <a class="read-more" href="pdfs/tahoe2018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                     </center>
 					 </div>

 					 <div class="item news-item">
	                   	 <center>
							 <a href="pdfs/cruze2018.pdf" target="_blank"><img alt="Cruze 2018" src="images/autos/cruze2018.jpg"></a>
							  <h2> <strong> <em> CRUZE <sup>&reg;</sup> <font color="#f5bc05">2018</font> </em> </strong></h2> </font>  
							 <a class="read-more" href="pdfs/cruze2018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                     </center>
 					 </div>

 					 <div class="item news-item">
	                   	 <center>
							 <a href="pdfs/trax2018.pdf" target="_blank"><img alt="Trax 2018" src="images/autos/trax2018.jpg"></a>
							  <h2> <strong> <em> TRAX <sup>&reg;</sup> <font color="#f5bc05">2018</font> </em> </strong></h2> </font>  
							 <a class="read-more" href="pdfs/trax2018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                     </center>
 					 </div>

 					   <div class="item news-item">
	                   	 <center>
							 <a href="pdfs/equinox2018.pdf" target="_blank"><img alt="Equinox 2018" src="images/autos/equinox2018.jpg"></a>
							  <h2> <strong> <em> EQUINOX <sup>&reg;</sup> <font color="#f5bc05">2018</font> </em> </strong></h2> </font>  
							 <a class="read-more" href="pdfs/equinox2018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                     </center>
 					 </div>

 					 <div class="item news-item">
	                   	 <center>
							 <a href="pdfs/tornado2018.pdf" target="_blank"><img alt="Tornado 2018" src="images/autos/tornado2018.jpg"></a>
							  <h2> <strong> <em> TORNADO <sup>&reg;</sup> <font color="#f5bc05">2018</font> </em> </strong></h2> </font>  
							 <a class="read-more" href="pdfs/tornado2018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                     </center>
 					 </div>

 					 <div class="item news-item">
	                   	 <center>
							 <a href="pdfs/colorado2018.pdf" target="_blank"><img alt="Colorado 2018" src="images/autos/colorado2018.jpg"></a>
							  <h2> <strong> <em> COLORADO <sup>&reg;</sup> <font color="#f5bc05">2018</font> </em> </strong></h2> </font>  
							 <a class="read-more" href="pdfs/colorado2018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                     </center>
 					 </div>

 					

 					 <div class="item news-item">
	                   	 <center>
							 <a href="pdfs/traverse2018.pdf" target="_blank"><img alt="Traverse 2018" src="images/autos/traverse2018.jpg"></a>
							  <h2> <strong> <em> TRAVERSE <sup>&reg;</sup> <font color="#f5bc05">2018</font> </em> </strong></h2> </font>  
							 <a class="read-more" href="pdfs/traverse2018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                     </center>
 					 </div>

 					  <div class="item news-item">
	                   	 <center>
							 <a href="pdfs/silverado15002018.pdf" target="_blank"><img alt="Slverado 1500 2018" src="images/autos/silverado2500.jpg"></a>
							  <h2> <strong> <em> SILVERADO 1500 <sup>&reg;</sup> <font color="#f5bc05">2018</font> </em> </strong></h2> </font>  
							 <a class="read-more" href="pdfs/silverado15002018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                     </center>
 					 </div>

 					 <div class="item news-item">
	                   	 <center>
							 <a href="pdfs/silverado25002018.pdf" target="_blank"><img alt="Slverado 2500 2018" src="images/autos/silverado2500.jpg"></a>
							  <h2> <strong> <em> SILVERADO 2500 <sup>&reg;</sup> <font color="#f5bc05">2018</font> </em> </strong></h2> </font>  
							 <a class="read-more" href="pdfs/silverado25002018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                     </center>
 					 </div>

 					 <div class="item news-item">
	                   	 <center>
							 <a href="pdfs/silverado35002018.pdf" target="_blank"><img alt="Silverado 3500 2018" src="images/autos/silverado3500.jpg"></a>
							  <h2> <strong> <em> SILVERADO 3500 <sup>&reg;</sup> <font color="#f5bc05">2018</font> </em> </strong></h2> </font>  
							 <a class="read-more" href="pdfs/silverado35002018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                     </center>
 					 </div>

 					  <div class="item news-item">
	                   	 <center>
							 <a href="pdfs/express2018.pdf" target="_blank"><img alt="Express 2018" src="images/autos/express2018.jpg"></a>
							  <h2> <strong> <em> EXPRESS <sup>&reg;</sup> <font color="#f5bc05">2018</font> </em> </strong></h2> </font>  
							 <a class="read-more" href="pdfs/express2018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                     </center>
 					 </div>
           
                </div>

                <br>

			</div>
            <!--FIN CONTENEDOR COCHES-->