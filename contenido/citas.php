
				<div class="services-box">
					<div class="container">
						<div class="row">
							<div class="col-md-3" >
								<div class="services-post">
									<a class="services-icon1" href="contacto.php" target="_self"><i class="fa fa-money"></i></a>
									<div class="services-post-content">
										<h4><strong>COTIZA TU AUTO</strong></h4>
										<p>Solicita una cotización, eligiendo el modelo de tu preferencia, la forma de pago y el plazo del financiamiento.</p>
									</div>
								</div>
							</div>
                            
							<div class="col-md-3" >
								<div class="services-post">
									<a class="services-icon1" href="contacto.php" target="_self"><i class="fa fa-check-circle"></i></a>
									<div class="services-post-content">
										<h4><strong>CITA DE MANEJO</strong></h4>
										<p>Entra a éste enlace para agendar tu cita de manejo en el vehículo de tu preferencia.</p>
									</div>
								</div>
							</div>                            

							<div class="col-md-3" >
								<div class="services-post">
									<a class="services-icon2" href="servicio.php" target="_self"><i class="fa fa-gears"></i></a>
									<div class="services-post-content">
										<h4><strong>CITA DE SERVICIO</strong></h4>
										<p>Entra aquí para fijar una cita de servicio para tu auto en cualquiera de nuestras agencias FAME</p>
									</div>
								</div>
							</div>
                            
							<div class="col-md-3" >
								<div class="services-post">
									<a class="services-icon1" href="refacciones.php" target="_self"><i class="fa fa-wrench"></i></a>
									<div class="services-post-content">
										<h4><strong>REFACCIONES</strong></h4>
										<p>Entra para solicitar una cotización de refacciones que necesite tu vehículo.</p>
									</div>
								</div>
							</div>                            
                            
						</div>
					</div>
					<img class="shadow-image" alt="" src="images/shadow.png">
				</div>
                
<!--FIN CITAS-->         

				<!-- ENLACES -->
				<div class="services-box">
					<div class="container">
						<div class="row">
							<div class="col-md-6">
								<div class="services-post">
									<a class="services-icon1" href="blog.php" target="_self"><img src="images/logo-blog.png"></a>
									<div class="services-post-content">
										<h4><a href="blog.html"><strong>#AutoFAME Blog</strong></a></h4>
										<p>Entérate de todas las novedades de la industria automotriz, aplica los tips y recomendaciones que tenemos para el máximo cuidado de tu automóvil.</p>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="services-post">
									<a class="services-icon2" href="http://www.fameseminuevos.com/" target="_blank"><img src="images/seminuevos.png"></a>
									<div class="services-post-content">
										<h4><a href="http://www.fameseminuevos.com/" target="_blank"><strong>FAME SEMINUEVOS</strong></a></h4>
										<p>Entra a nuestra sección para encontrar tu próxima unidad seminueva, certificada y garantizada por nuestros asesores de servicio calificados.</p>
									</div>
								</div>
							</div>
                            
						</div>
					</div>
				<img class="shadow-image" alt="" src="images/shadow.png">
				</div>

			</div>

		</div>